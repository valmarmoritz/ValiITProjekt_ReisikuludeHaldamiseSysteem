
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 09/13/2017 09:55:10
-- Generated from EDMX file: C:\Users\merli\source\repos\ValiITProjekt_ReisikuludeHaldamiseSysteem\ValiIT_ReisikuludeHaldamiseSysteem\ReisikuludeDM.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Northwind];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Töötaja]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Töötaja];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Töötaja'
CREATE TABLE [dbo].[Töötaja] (
    [Isikukood] char(11)  NOT NULL,
    [Eesnimi] nvarchar(20)  NOT NULL,
    [Perekonnanimi] nvarchar(50)  NOT NULL,
    [Ametikoht] nchar(30)  NULL,
    [OtseneÜlemus] char(11)  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Isikukood] in table 'Töötaja'
ALTER TABLE [dbo].[Töötaja]
ADD CONSTRAINT [PK_Töötaja]
    PRIMARY KEY CLUSTERED ([Isikukood] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------