﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValiIT_ReisikuludeHaldamiseSysteem
{
    class Program
    {
        static Töötaja kasutaja;
        static string oigused;
        static bool KasutajaOnAktiivne;
        static bool KasutajaOnIseÜlemus;
        static bool KasutajalOnÜlemus;
        static bool KasutajaOnRaamatupidaja;
        static void Main(string[] args)
        {
            using (ReisikuludeHaldamiseSysteemVMEntities1  RHS = new ReisikuludeHaldamiseSysteemVMEntities1())
            {
                //testime andmebaasi lugemist
                //Console.WriteLine("Testime andmebaasi lugemist...");
                //foreach (var x in RHS.Töötaja)
                //{
                //    Console.Write(x);
                //    if (x.OtseneÜlemus != null)
                //    { Console.WriteLine($", tema ülemus on {RHS.Töötaja.Where(y => y.Isikukood == x.OtseneÜlemus).Single()}"); }
                //    else Console.WriteLine(", tal ei ole ülemust");
                //}

                //sisse logimine ja kasutajaõiguste kontroll
                Console.Clear();
                Console.WriteLine("Reisikulude Haldamise Systeem v.0.02 (R)2017 M&V Solutions Inc\n");
                string kasutajaIsikukood;
                bool sisseLogitud = false;

                while (true)
                {
                    kasutajaIsikukood = "";
                    KasutajaOnAktiivne = false;
                    do
                    {
                        Console.Write("Palun sisesta oma isikukood (väljumiseks <Enter>): ");
                        kasutajaIsikukood = Console.ReadLine();
                        kasutaja = RHS.Töötaja.Where(x => x.Isikukood == kasutajaIsikukood).SingleOrDefault() ?? null;
                        if (kasutajaIsikukood == "") { Console.WriteLine("Head aega!"); Environment.Exit(0); }
                        else if (kasutaja == null) Console.WriteLine("VIGA! Sellist isikukoodi meie andmebaasis ei ole!");
                        else KasutajaOnAktiivne = RHS.Töötaja.Where(x => x.Isikukood == kasutaja.Isikukood).Single().OnAktiivne;
                        if (!KasutajaOnAktiivne) Console.WriteLine("Sul pole kasutajaõigusi.");
                    }
                    while (!KasutajaOnAktiivne || kasutaja == null);

                    OigusteKontroll(kasutaja);
                    sisseLogitud = true;
                    Console.Clear();
                    Console.WriteLine($"Tere, {kasutaja}.");

                    // kasutaja tööpaneel
                    do
                    {
                        Console.WriteLine($"\nSa saad: {oigused}");
                        switch (Console.ReadKey(true).KeyChar.ToString().ToUpper())
                        {
                            case "A":
                                Console.Clear();
                                Console.WriteLine("\nReisikulude aruannete esitamine");
                                JargmisesVersioonis();
                                break;
                            case "K":
                                Console.Clear();
                                Console.WriteLine("\nMinu alluvate reisikulude aruannete kinnitamine");
                                if (!KasutajaOnIseÜlemus) Console.WriteLine("Alluvaid ei ole...");
                                else
                                {
                                    JargmisesVersioonis();
                                }
                                break;
                            case "L":
                                Console.Clear();
                                Console.WriteLine("\nMinu esitatud reisikulude aruannete kinnitamine");
                                if (KasutajalOnÜlemus) Console.WriteLine("Selle jaoks on ülemus...");
                                else
                                {
                                    JargmisesVersioonis();
                                }
                                break;
                            case "P":
                                Console.Clear();
                                Console.WriteLine("\nReisikulu aruannete hüvitamine");
                                if (!KasutajaOnRaamatupidaja) Console.WriteLine("Seda saab teha ainult raamatupidaja...");
                                else
                                {
                                    JargmisesVersioonis();
                                }
                                break;
                            case "X":
                                Console.WriteLine("\nX - logi välja");
                                sisseLogitud = false;
                                break;
                            default:
                                Console.Clear();
                                Console.WriteLine();
                                Console.WriteLine("Ei saanud aru. Palun vali toiming, vajutades klahvile, mis on rea ees nurksulgudes.");
                                break;
                        }
                    }
                    while (sisseLogitud);
                }
            }
        }
        public static void OigusteKontroll(Töötaja kasutaja)
        {
            using (ReisikuludeHaldamiseSysteemVMEntities1 RHS = new ReisikuludeHaldamiseSysteemVMEntities1())
            {
                KasutajalOnÜlemus = RHS.Töötaja.Where(x => x.Isikukood == kasutaja.OtseneÜlemus).Count() > 0;
                KasutajaOnIseÜlemus = RHS.Töötaja.Where(x => x.OtseneÜlemus == kasutaja.Isikukood).Count() > 0;
                KasutajaOnRaamatupidaja = RHS.Töötaja.Where(x => x.Isikukood == kasutaja.Isikukood).Single().OnRaamatupidaja ;

                StringBuilder sb = new StringBuilder();
                sb.Append("\n[A] esitada reisikulude aruandeid");
                if (KasutajaOnIseÜlemus) sb.Append("\n[K] kinnitada oma alluvate reisikulude aruandeid");
                if (!KasutajalOnÜlemus) sb.Append("\n[L] kinnitada omaenda reisikulude aruandeid");
                if (KasutajaOnRaamatupidaja) sb.Append("\n[P] hüvitada kinnitatud reisikulude aruandeid");
                sb.Append("\n[X] välja logida");
                oigused = sb.ToString();
            }
        }
        public static void JargmisesVersioonis()
        {
            Console.WriteLine("... see funktsionaalsus realiseeritakse mõnes järgnevas versioonis ...");
        }
    }
    public partial class Töötaja
    {
        public override string ToString()
        {
            return $"{Ametikoht.Trim()} {Eesnimi.Trim()} {Perekonnanimi.Trim()}"; 
        }
    }
}
